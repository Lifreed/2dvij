import sys, pygame, math, timeit
from func import *
pygame.init()

size = width, height = 850, 600
black = 0, 0, 0
white = 255, 255, 255

speed = [1000, 1000]
speed_two = [-1000, 1000]
speed_tree = [1, -1]

mass = 1
mass_two = 2
mass_three = 3

acc = (0, 10)
friction = 0

radius = 20
radius_two = 30
radius_three = 40

vector = [0, 0]
vector_two = [790, 0]
vector_three = [0, 520]


screen = pygame.display.set_mode(size)


ball = pygame.image.load("ball.png")
ball_two = pygame.image.load("ball_2.png")
ball_three = pygame.image.load("ball_3.png")

first_ball = PhysicalObject(vector, speed, [acc], radius, mass, friction)
second_ball = PhysicalObject(vector_two, speed_two, [acc], radius_two, mass_two, friction)
#hird_ball = PhysicalObject(vector_three, speed_tree, [acc], radius_three, mass_three)
engine = Engine(width, height)
engine.add_object(first_ball)
engine.add_object(second_ball)
#engine.add_object(third_ball)
time = timeit.timeit("engine.update(0.00005)", setup="from __main__ import engine", number=1)
t = 0.0
dt = 0.001

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
    while dt > t:
        time = timeit.timeit("engine.update(time)", setup="from __main__ import engine, time", number=1)
        t += time
    t -= dt
    #engine.update(time)
    screen.fill(black)
    # pygame.draw.circle(screen, white, first_ball.position, first_ball.radius, 5)
    screen.blit(ball, first_ball.position)
    screen.blit(ball_two, second_ball.position)
    #screen.blit(ball_three, third_ball.position)
    pygame.display.flip()

