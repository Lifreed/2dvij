import sys, pygame, math
import itertools
pygame.init()


class Engine:
    def __init__(self, width, height):
        self.height = height
        self.width = width
        self.objects = []

    def add_object(self, object):
        self.objects.append(object)

    def update(self, time):
        dt = 1 / time
        for obj_a, obj_b in itertools.combinations(self.objects, 2):
            obj_a.collision(obj_b, dt)
            obj_a.update(self.width, self.height, dt)
            obj_b.update(self.width, self.height, dt)


class PhysicalObject:
    def __init__(self, position, speed, accelerations, radius, mass, friction=0.0001, loss_coef = 0.75):
        self.position = position
        self.accelerations = accelerations
        self.speed = speed
        self.radius = radius
        self.mass = mass
        self.friction = friction
        self.loss_coef = 0.75

    def update(self, width, height, dt):
        self.elasticity(height, width, dt)
        self.accelerate()
        self.friction_force()
        self.move(dt)
        print(self.speed[0])
        # print(self.position)

    def move(self, dt):
        self.position[0] += (self.speed[0] / dt)
        self.position[1] += (self.speed[1] / dt)

    def accelerate(self):
        for acc in self.accelerations:
            self.speed[0] += acc[0]
            self.speed[1] += acc[1]

    def friction_force(self):
        sign_1 = 1
        sign_2 = 1
        if self.speed[0] < 0:
            sign_1 = -1
        if self.speed[1] < 0:
            sign_2 = -1
        self.speed[0] = (math.fabs(self.speed[0]) - self.friction) * sign_1
        self.speed[1] = (math.fabs(self.speed[1]) - self.friction) * sign_2

    def elasticity(self, height, width, dt):
        if math.fabs(self.speed[0]) < 100:  # Остановка по OX при очень маленькой скорости
            self.speed[0] = 0
        if self.position[0] + (self.speed[0] / dt) < 0:  # Удар об левую грань
            self.position[0] = 0
        elif self.position[0] + (self.speed[0] / dt) > width - 2 * self.radius:  # Удар об правую грань
            self.position[0] = width - 2 * self.radius
        if self.position[0] + (self.speed[0] / dt) < 0 or \
                self.position[0] + (self.speed[0] / dt) > width - 2 * self.radius:
            self.speed[0] = - self.speed[0] * self.loss_coef
        if self.position[1] + (self.speed[1] / dt) < 0:  # Удар об верхнюю граню
            self.position[1] = 0
        if self.position[1] + (self.speed[1] / dt) > height - 2 * self.radius:  # Удар об нижнюю грань
            self.position[1] = height - 2 * self.radius
        if self.position[1] + (self.speed[1] / dt) < 0 or \
                self.position[1] + (self.speed[1] / dt) > height - 2 * self.radius:
            self.speed[1] = -self.speed[1] * self.loss_coef

    def collision(self, anoterObj, dt):
        if (
            math.fabs((self.position[0] + (self.speed[0] / dt)) -
                      math.fabs(anoterObj.position[0] + (anoterObj.speed[0] / dt)))) ** 2 + \
                      (math.fabs((self.position[1] + (self.speed[1] / dt)) - math.fabs(anoterObj.position[1] +
                      (anoterObj.speed[1] / dt)))) ** 2 < (self.radius + anoterObj.radius) ** 2:
            try:
                phi = math.atan2((self.position[1] - anoterObj.position[1]), (self.position[0] - anoterObj.position[0]))
            except ZeroDivisionError:
                phi = math.asin(1)
            try:
                theta1 = math.atan2(self.speed[1], self.speed[0])
            except ZeroDivisionError:
                theta1 = math.asin(1)
            try:
                theta2 = math.atan2(anoterObj.speed[1], anoterObj.speed[0])
            except ZeroDivisionError:
                theta2 = math.asin(1)
            s3 = math.sqrt(self.speed[0] ** 2 + self.speed[1] ** 2) * math.cos(theta1 - phi)
            s2 = math.sqrt(self.speed[0] ** 2 + self.speed[1] ** 2) * math.sin(theta1 - phi)
            s1 = math.sqrt(anoterObj.speed[0] ** 2 + anoterObj.speed[1] ** 2) * math.cos(theta2 - phi)
            s4 = math.sqrt(anoterObj.speed[0] ** 2 + anoterObj.speed[1] ** 2) * math.sin(theta2 - phi)
            self.speed[0] = s1 * math.cos(phi) - s2 * math.sin(phi)
            self.speed[1] = s1 * math.sin(phi) + s2 * math.cos(phi)
            anoterObj.speed[0] = s3 * math.cos(phi) - s4 * math.sin(phi)
            anoterObj.speed[1] = s3 * math.sin(phi) + s4 * math.cos(phi)


class Rect:
    def __init__(self, size_rect, fix_size, speed, angle, accelerations, friction=0):
         self.position_one = size_rect[0]
         self.position_two = size_rect[1]
         self.position_center = size_rect[2]
         self.speed = speed
         self.accelerations = accelerations
         self.friction = friction
         self.angle = angle
         self.position = [size_rect[0], size_rect[1], [0, 0], [0, 0]]
         self.fix_size = fix_size
         self.new_angle = 0

    def update(self, height, width):
         self.rotate()
         self.move()
         self.make_rect()
         self.elasticity(height, width)
         print(self.position)
         print(self.position_center)
         print(self.fix_size)
         print(self.new_angle)

    def move(self):
        self.position_one[0] += self.speed[0]
        self.position_one[1] += self.speed[1]
        self.position_two[0] += self.speed[0]
        self.position_two[1] += self.speed[1]
        self.position_center[0] += self.speed[0]
        self.position_center[1] += self.speed[1]
        self.fix_size[0][0] += self.speed[0]
        self.fix_size[0][1] += self.speed[1]
        self.fix_size[1][0] += self.speed[0]
        self.fix_size[1][1] += self.speed[1]

    def rotate(self):
         self.new_angle += self.angle
         self.new_angle = round(self.new_angle, 3)
         if self.new_angle > 6.28:
            self.new_angle = round(self.new_angle - 6.28, 3)
            self.position_one[0] = ((self.fix_size[0][0] - self.position_center[0]) * math.cos(self.angle + self.new_angle) - \
                                (self.fix_size[0][1] - self.position_center[1]) * math.sin(self.angle + self.new_angle)) + self.position_center[0]
            self.position_two[0] = ((self.fix_size[1][0] - self.position_center[0]) * math.cos(self.angle + self.new_angle) - \
                                (self.fix_size[1][1] - self.position_center[1]) * math.sin(self.angle + self.new_angle)) + self.position_center[0]
            self.position_one[1] = ((self.fix_size[0][0] - self.position_center[0]) * math.sin(self.angle + self.new_angle) + \
                                (self.fix_size[0][1] - self.position_center[1]) * math.cos(self.angle + self.new_angle)) + self.position_center[1]
            self.position_two[1] = ((self.fix_size[1][0] - self.position_center[0]) * math.sin(self.angle + self.new_angle) + \
                                (self.fix_size[1][1] - self.position_center[1]) * math.cos(self.angle + self.new_angle)) + self.position_center[1]
         else:
            self.position_one[0] = ((self.position_one[0] - self.position_center[0]) * math.cos(self.angle) - \
                                (self.position_one[1] - self.position_center[1]) * math.sin(self.angle)) + self.position_center[0]
            self.position_two[0] = ((self.position_two[0] - self.position_center[0]) * math.cos(self.angle) - \
                                (self.position_two[1] - self.position_center[1]) * math.sin(self.angle)) + self.position_center[0]
            self.position_one[1] = ((self.position_one[0] - self.position_center[0]) * math.sin(self.angle) + \
                                (self.position_one[1] - self.position_center[1]) * math.cos(self.angle)) + self.position_center[1]
            self.position_two[1] = ((self.position_two[0] - self.position_center[0]) * math.sin(self.angle) + \
                                (self.position_two[1] - self.position_center[1]) * math.cos(self.angle)) + self.position_center[1]

    def make_rect(self):
        self.position[0] = self.position_one
        self.position[1] = self.position_two
        self.position[2][0] = 2 * self.position_center[0] - self.position_one[0]
        self.position[2][1] = 2 * self.position_center[1] - self.position_one[1]
        self.position[3][0] = 2 * self.position_center[0] - self.position_two[0]
        self.position[3][1] = 2 * self.position_center[1] - self.position_two[1]

    def elasticity(self, height, width):
        max_x = min_x = self.position[0][0]
        max_y = min_y = self.position[0][1]
        for i in range(0, 4):
            if self.position[i][0] > max_x:
                max_x = self.position[i][0]
            if self.position[i][0] < min_x:
                min_x = self.position[i][0]
            if self.position[i][1] > max_y:
                max_y = self.position[i][1]
            if self.position[i][1] < min_y:
                min_y = self.position[i][1]
        if max_x + self.speed[0] > width or min_x + self.speed[0] < 0:
            self.speed[0] = -self.speed[0]
        if max_y + self.speed[1] > height or min_y + self.speed[1] < 0:
            self.speed[1] = -self.speed[1]