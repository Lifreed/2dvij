from func import *
import unittest
import math
import random


class Test(unittest.TestCase):
    def test_move(self):
        obj_a = PhysicalObject([800, 600], [5, 10], [0, 0], 10, 1)

        PhysicalObject.move(obj_a)

        self.assertEqual(obj_a.position, [805, 610])

    def test_accelerations(self):
        obj_a = PhysicalObject([800, 600], [5, 10], [[0.02, 0.98]], 10, 1)

        PhysicalObject.accelerate(obj_a)

        self.assertEqual(obj_a.speed, [5.02, 10.98])

    def test_friction_force(self):
        obj_a = PhysicalObject([800, 600], [5, 10], [0, 0], 10, 1)
        obj_a.fric = 0.1

        PhysicalObject.friction_force(obj_a)

        self.assertEqual(obj_a.speed, [4.9, 9.9])

    def test_elasticity_no_evet(self):
        obj_a = PhysicalObject([500, 500], [40, 20], [0, 0], 100, 1)
        height = 1920
        width = 1080

        PhysicalObject.elasticity(obj_a, height, width)

        self.assertEqual(obj_a.position, [500, 500])
        self.assertEqual(obj_a.speed, [40, 20])

    def test_elasticity_xw(self):
        obj_a = PhysicalObject([855, 500], [40, 20], [0, 0], 100, 1)
        height = 1920
        width = 1080

        PhysicalObject.elasticity(obj_a, height, width)

        self.assertEqual(obj_a.position[0], 880)
        self.assertEqual(obj_a.speed, [-30, 20])

    def test_elasticity_yh(self):
        obj_a = PhysicalObject([500, 855], [20, 40], [0, 0], 100, 1)
        height = 1080
        width = 1920

        PhysicalObject.elasticity(obj_a, height, width)

        self.assertEqual(obj_a.position[1], 880)
        self.assertEqual(obj_a.speed, [20, -30])

    def test_elasticity_x0(self):
        obj_a = PhysicalObject([20, 500], [-40, 20], [0, 0], 100, 1)
        height = 1920
        width = 1080

        PhysicalObject.elasticity(obj_a, height, width)

        self.assertEqual(obj_a.position[0], 0)
        self.assertEqual(obj_a.speed, [30, 20])

    def test_elasticity_y0(self):
        obj_a = PhysicalObject([500, 20], [20, -40], [0, 0], 100, 1)
        height = 1080
        width = 1920

        PhysicalObject.elasticity(obj_a, height, width)

        self.assertEqual(obj_a.position[1], 0)
        self.assertEqual(obj_a.speed, [20, 30])

    def test_collision_no_event(self):
        obj_a = PhysicalObject([0, 0], [2, 3], [0, 0], 10, 1)
        obj_b = PhysicalObject([500, 500], [-2, 3], [0, 0], 10, 1)

        PhysicalObject.collision(obj_a, obj_b)

        self.assertEqual(obj_a.speed, [2, 3])
        self.assertEqual(obj_b.speed, [-2, 3])

    def test_collision_Zero1(self):
        obj_a = PhysicalObject([100, 100], [5, 5], [0, 0], 10, 1)
        obj_b = PhysicalObject([120, 120], [-5, -5], [0, 0], 10, 1)

        PhysicalObject.collision(obj_a, obj_b)

        self.assertEqual([-round(obj_a.speed[0]), -round(obj_a.speed[1])] , [round(obj_b.speed[0]), round(obj_b.speed[1])])

    def test_collision_equal(self):
        obj_a = PhysicalObject([100, 120], [5, 5], [0, 0], 10, 1)
        obj_b = PhysicalObject([130, 120], [-5, -5], [0, 0], 20, 1)

        PhysicalObject.collision(obj_a, obj_b)

        self.assertEqual(math.sqrt(math.fabs(obj_a.position[0] - obj_b.position[0]) ** 2 +
                                   math.fabs(obj_a.position[1] - obj_b.position[1]) ** 2), obj_b.radius + obj_a.radius)

    def test_collision_less(self):
        obj_a = PhysicalObject([100, 120], [5, 5], [0, 0], 10, 1)
        obj_b = PhysicalObject([120, 120], [-5, -5], [0, 0], 20, 1)

        PhysicalObject.collision(obj_a, obj_b)

        self.assertLess(math.sqrt(math.fabs(obj_a.position[0] - obj_b.position[0]) ** 2 +
                                   math.fabs(obj_a.position[1] - obj_b.position[1]) ** 2), obj_b.radius + obj_a.radius)

    def test_collision_greater(self):
        obj_a = PhysicalObject([100, 120], [5, 5], [0, 0], 10, 1)
        obj_b = PhysicalObject([140, 120], [-5, -5], [0, 0], 20, 1)

        PhysicalObject.collision(obj_a, obj_b)

        self.assertGreater(math.sqrt(math.fabs(obj_a.position[0] - obj_b.position[0]) ** 2 +
                                   math.fabs(obj_a.position[1] - obj_b.position[1]) ** 2), obj_b.radius + obj_a.radius)


if __name__ == "__main__":
    unittest.main()