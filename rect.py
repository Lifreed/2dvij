import sys, pygame, math
from func import *
pygame.init()

size = width, height = 800, 600
black = 0, 0, 0
white = 255, 255, 255

position = [400, 200]
#position_two = [810, 0]
#position_three = [0, 560]

size_rect = [[400, 200], [460, 200], [440, 220]]
fix_size = [[400, 200], [460, 200]]



speed = [1 , 1]
#speed_two = [-1, 1]
#speed_three = [1, -1]


angle = 0.005


acc = (0, 0)
friction = 0


screen = pygame.display.set_mode(size)


first_rect = Rect(size_rect, fix_size, speed, angle,  acc)

engine = Engine(width, height)
engine.add_object(first_rect)

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
    Rect.update(first_rect, height, width)
    screen.fill(black)
    pygame.draw.lines(screen, white, True, first_rect.position, 5)
    pygame.display.flip()